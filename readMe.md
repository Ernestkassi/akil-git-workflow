# Documentation :star2:

Document git sur le travail a Akil Technologies

#### SOMMAIRE

- [Le workflow avec git et gitlab dans vos différents projets.](#le-workflow-avec-git-et-gitlab-dans-vos-différents-projets.)
- [Pusher mon code](#pusher-mon-code)
- [Faire valider son travail : Comment ?](#faire-valider-son-travail)
- [Comment fait-on un Pull Pequest (`PR`) ?]()
- [Remarque](#remarque)
- [Contributeurs]()
- [Gestion des conflits]()

Grand merci à tous, pour vos réponses. Ce document est la matérialisation de ce que j'ai compris à travers celles-ci

### Pre-requis :

- Etre à l’aise avec Git
- Connaitre GitLab
- Etre un Akiller[^akiller] :smile:

### :one: Le workflow avec git et gitlab dans vos différents projets.

Si vous êtes déjà un habitué, ce document pourrait s'averer moins utile. Dans le cas contraire, vous êtes au bon endroit.

Ce que vous devez retenir est que lorsque vous `forker` un projet, celui-ci est étroitement lié avec le repository (dépôt) de base.
Pour forker un projet, cliquez sur le `fork` du projet en question comme le montre l'image ci-contre

![Forker un projet!](/img/forker.png "Cliquez sur le bouton avec l'icone en Y")

Vous verrez apparaitre le formulaire suivant. Remplissez-le et le projet fera partie de vos projets desormais

![Forker un projet!](/img/forker1.png "Cliquez sur le bouton avec l'icone en Y")

> ##### Vous pouvez maintenant cloner le projet afin de l’avoir localement
>
> - _git clone origin [lien de votre dépot]_
>
> ###### _Dans votre terminal, entrez le commande ci-dessus_.

Quand vous forkez un projet, il vient avec une branche (master) par défaut. C’est donc à partir de là, que vous allez commencer à travailler.

> ##### Vous devez lier maintenant votre dépôt local à celui que vous avez forker.
>
> - _git remote add upstream [lien du dépôt que vous avez forker]`_
>
> ###### _Dans votre terminal, entrez le commande ci-dessus_.

### NB : Pour chaque fonctionnalité à implémenter, vous devez créer une nouvelle `branche`

Appliquons ceci avec un exemple pour mieux voir. Le projet vient avec la branche master vous avais-je dis. D'emblée, à partir de la master, créez un nouvelle branche : `fonctionnalite1`. Ensuite déplacez-vous dans cette nouvelle branche. Travaillez
Avant de soumettre votre travail, n'oubliez pas d'incrémenter la version de votre code. le versioning va permettre à git de suivre votre progression

> ##### Créez branche, Travailler
>
> - _git checkout -b `fonctionnalite1`_
> - _git add ._
> - _git commit -m "`message du commit`"_
>
> ###### _Dans votre terminal_. _A la fin n'oubliez pas de faire le versioning de votre code_
>
> - _yarn build:version_

### :two: Pusher mon code

Une fois que vous avez terminé ce que vous avez à faire, `pusher` votre travail sur votre dépôt distant.

> ##### Vous pouvez maintenant cloner le projet afin de l’avoir localement
>
> - _git push -f origin `fonctionnalite1`_
>
> ###### _Dans votre terminal, entrez le commande ci-dessus_.

### :three: Faire valider son travail : Comment ?

Une autre chose à savoir est qu’ici (`À Akil`), ce n'est pas à vous de valider votre propre travail. A moins que vous soyez un `Master`[^master] Et le processus de validation de votre travail passe par un `merge request (MR)`. Ne vous inquietez pas quand vous entendrez parler de `PR`. Il s'agit en fait de `Pull Request`. C'est la même chose. Cela depend du fait que vous êtes sur `github` ou `gitlab`

### :four: Comment fait-on un Pull Request (`PR`) ?

Après un `push`, git vous fournit un lien qui ressemble à peu près à ce qui suit :
remote : https://gitlab.com/userName/projectName/-/merge_requests/new?merge_request%5Bsource_branch%5D=branchName

Cliquez sur ce lien puis renseignez les informations demandées. Ces informations concernent `Assignee` (le nom github/gitlab de votre Master), `Reviewer`(le nom github/gitlab de votre Master)... `Milestone`, `Labels` peuvent rester par défaut. Une fois renseignées, cliquez sur le bouton `create merge request`. Attendez donc que votre Master valide votre travail ou vous fasse un retour par rapport à ce que vous avez fait de pas bien et/où il vous propose une amélioration.

### :warning: Sachez qu' un Master a beaucoup à faire. :warning:

Donc en attendant qu’il valide votre travail, vous devez continuer de travailler. Créez donc une nouvelle branche `fonctionnalite2` depuis la branche `fonctionnalite1`. Continuez de travailler _`git add . - git commit -m "message du commit")`_ comme d’habitude.

##### Lorsque vous allez vouloir faire valider votre travail, c’est la branche `fonctionnalite2` que vous devez push cette fois-ci... (_Cf :three:_)

##### Retenz que celle-ci est la plus à jour.

NB : lorsque la fonctionnalité à implémenter dans la branche `fonctionnalite2` dépend de la branche `fonctionnalite1` créez donc la nouvelle branche à partir de l’ancien. Dans le cas contraire revenez dans la branche `master` grâce à un checkout.

> - _git checkout master_

Lorsque vous que le ou les travaux précédant ont été validés par votre Master, vous devez récupérer cela en local. Pour le faire, assurez-vous que la branche dans laquelle vous êtes en train de travailler est clean. Vérifiez cela avec la commande suivante

> _git status_

Au cas où vous avez fait des modifications, validez-les avec les commandes

> - _git add ._
> - _git commit -m ‘message du commit’_
>
> ###### Dans votre terminal, entrez les commandes ci-dessus

Déplacez-vous ensuite dans votre branche principale. Dans votre cas, c’est la master. Dans votre terminal, entrez ensuite la commande

> - _git pull --rebase upstream master_.

Cela va récupérer le projet que vous aveczforkez et qui est à jour avec le bon code pour mettre à jour votre master local.
De la, créer à nouveau une nouvelle branche, `fonctionnalite3`. Et le processus reprend.

### :five: Remarque

Il peut arriver que votre travail ressemble à ceci : De votre branche master vous avez créé une première branche (vous avez pushe et fait un PR), une deuxième branche depuis la première (que vous avez pushe et fait un PR également). Puis une troisième, une quatrième...une n-ième. En attendant que le master merge ou vous fasse un retour pour d'éventuelles corrections, créez une (n-ième + 1) (depuis la n-ième) dans laquelle vous allez continuer de travailler.

Supposons que chemin faisant, il vous fait un retour pour le premier PR et un quelconque autre, vous avez deux choix qui s'offre a vous (ces methodes consiste tous à mettre en attente ce que vous êtes entrain de faire) :

> _Faire un push puis un PR anticipé en le mettant au brouillon_ > _Mettre en cache vos fichiers modifiés et ajoutés_

##### :heavy_minus_sign: Faire un PR anticipé

D'abord vous faite `git add . && git commit`, pushez et faites un PR "anticipé". Ce PR a la possibilité de prendre un état `brouillon (WIP)`[^wip] qui veut dire que vous continuez de travailler. Ainsi, gitlab va désactiver le bouton qui permet à votre Master de merger. Il pourra certes voir ce que vous avez fait et c’est tout.

Ensuite dès que vous finissez de corriger ( _`nous allons en parler un peu plus tard`_ ), revenez dans la branche du PR anticipé pour continuer et apres push sur la même branche. Ceci mettra à jour votre branche distante.

Enfin allez-y sur le PR en question, vous y trouverai un bouton comme l'indique l'image suivante. Le votre aura le message `Mark as ready` et puis c'est bon

> ![Mark as ready](/img/mark-as-ready.png "Cliquez sur le bouton Mark as ready")

##### :heavy_minus_sign: Mettre en cache vos fichiers

Cette manière permet de mettre en cache tous vos fichiers et après de les recupérer grâce à une autre commande. Dans votre terminal, entrez les commandes suivantes :

J’espère que ce document vous aura aidé à comprendre quelque chose. Si vous avez des incompréhensions, n'hésitez pas approcher n’importe quel Killer, il vous aidera avec plaisir

> - _git stash_
>
> ###### Lorsque vous serez de retour la branche, entrez la commande
>
> - _git stash pop_

Dans les deux cas, après avoir fait le PR ou mise ne cache, déplacez vous dans la branche dans laquelle l'erreur a été signalé par votre Master, puis entrez la commande suivante :

> - _git pull --rebase upstream master_
> - _git status_

Resolvez vos conflits puis entrez ces conmandes :

> - _git add ._
> - _git rebase --continue_
>
> ###### Dans votre terminal, entrez les commandes ci-dessus
>
> ###### Faites un PR sur la même branche

### :six: Corrections suivant les commentaires fait sur un PR

Après avoir fait un PR, le validateur de votre code (ici, votre Master) peut faire des commentaires sur votre code. Ces commentaires peuvent concerner votre manière de coder, un algorithme particulier, des lignes qui ne servent à rien. Bref, des critiques liés à votre code.
Pour corriger ceux-ci, vous devez vous déplacer dans la branche en question, puis entrez la commande suivante :

> - _git reset HEAD~1_
>
> ###### Cette commande va annuler votre dernier `commit` et et vous reverez vos fichiers (avec leurs états modifié ou ajouté ) exactement comme ils étaient avant de commiter.

Après modification, referz-vous à l'état :two:
Pour renvoyer votre code sur la même brancher en forçant

### :seven: Contributeurs

| Nom & Prenoms |     gitName     |        Rôle |
| :------------ | :-------------: | ----------: |
| KOUAME Yao    |   @kouameYao    |   Rédacteur |
| Hassan        |   @Al-Hassan    | Instructeur |
| Mouctar       |  @mouctarbah6   |    Reviewer |
| Patrice       |    @patrice     | Instructeur |
| Amara         | @amara.dianney  | Instructeur |
| Maxence       | @maxence_yrowah | Instructeur |
| Duverdier     |   duverdier1    | Instructeur |

## Forker ce projet et exercez-vous :computer:

[^master]: Un master, c'est le supérieur sur un projet. Certaines personnes l'appellent manager
[^wip]: Work In Proccess
[^akiller]: Être un développeur chez Akil Technologie
